// https://jsonplaceholder.typicode.com/todos

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos', {

	
	method: 'POST', 

	headers: {
		'Content-type': 'application/json'
	},
	
	body: JSON.stringify({
		completed: false,
		id: '201',
		title: 'Created To Do List Item',
		userId: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {

	
	method: 'PUT', 

	headers: {
		'Content-type': 'application/json'
	},
	
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list with a different data structure",
		id: '1',
		status: "Pending",
		title: 'Updated To Do List Item',
		userId: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {

	
	method: 'PATCH', 

	headers: {
		'Content-type': 'application/json'
	},
	
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: '1',
		status: "Complete",
		title: 'delectus aut autem',
		userId: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));

